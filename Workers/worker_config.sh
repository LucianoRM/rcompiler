#!/bin/bash

source .config

OUTPUT="#ifndef _CONN_CONF_H_
#define _CONN_CONF_H_

#define SERVER_WORKER_LISTENER_IP \"$SERVER_WORKER_LISTENER_IP\"
#define SERVER_WORKER_LISTENER_PORT $SERVER_WORKER_LISTENER_PORT
#define ARCHITECTURE $WORKER_ARCHITECTURE


#endif /* _CONN_CONF_H_ */"

echo -e "$OUTPUT" > conn_conf.h

make clean
make


mount $NFS_REMOTE_IP:$NFS_REMOTE_FOLDER $NFS_LOCAL_FOLDER

./server_remote_worker
