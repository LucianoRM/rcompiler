#include "tcp.h"
#include "conf.h"
#include "conn_conf.h"
#include "sys/types.h"
#include "sys/wait.h"



//Server remote worker. Receives a job from server indian, does it and send reply
int main() {

	//Ingore writeerror signals
	signal(SIGPIPE,SIG_IGN);

	//Create socket
	int socketfd = tcp_open_activo(SERVER_WORKER_LISTENER_IP,SERVER_WORKER_LISTENER_PORT);

	//Check for errors
	if(socketfd == -1){
		printf("Error connecting to server\n");
		perror(NULL);
		return 1;
	}

	//At this point, it should be connected to server

	//First, send architecture to server and register
	char architecture[2];
	sprintf(architecture,"%d\0",ARCHITECTURE);
	write(socketfd,architecture,2);



	//Then wait for server messages


	char message_from_indian[BLOCKSIZE];

	while(1){
		//Read message from indian
		tcp_read_or_die(socketfd,message_from_indian,BLOCKSIZE);
		int pid = fork();
        if(pid == 0){
            close(socketfd);
            execl("./exec.sh","exec.sh",message_from_indian,(void*)0);
            exit(-1);//If here is because execv had an error
        }
		waitpid(pid,NULL,0);

		/*/This part is done because inotify does not work with NFS unless the files are modified from the same computer

		//Notify server that worker has finished
		char message[2];
		sprintf(message,"a\0");
		write(socketfd,message,2);
		*/

	}
	close(socketfd);


	return 0;
}
