#!/bin/bash
source .config

NFS="./files"
TESTDIR=$NFS"/test"
LOG_FILE=$NFS"/log"


#parse parameters.Because of the way the script is called, all prameters are in $1.

VAR=(`echo $1 | tr ' ' '\n'`)
USER=${VAR[1]}
COMPILEDNAME=${VAR[3]}
DIR=$NFS"/"$USER
FILEPATH=$USER".tar"
RESULT=$DIR"/result"
TMP_FOLDER="./tmp"
RUN_FILE=${VAR[2]}
RUN_FILE=$DIR"/"$RUN_FILE


if [ ${VAR[0]} == "-c" ];then

	#Enter dir
	pushd $DIR


	#List files contained in tar
	FILES=$(tar -t -f $FILEPATH)

	#Search for file and uncompress it
	tar -xf $FILEPATH

	#Delete tar file
	rm -f $FILEPATH

	#default, error
	OUTPUT="1\n"


	#If Makefile present, run make and save output. If not run gcc
	if [ -f "Makefile" ];then
		OUTPUT+=$(make -s 2>&1) #should be like that because errors in make are shown via stderr
		ERROR=$?

		LOG="Compilo ["$FILES"] con errores"
	else
		OUTPUT+=$(gcc *.c -o $COMPILEDNAME 2>&1)
		ERROR=$?
		LOG="Compilo ["$FILES"] con errores"
	fi

	#check for errors
	if [ $ERROR -eq 0 ];then #No errors
		OUTPUT="0\n"
		LOG="Compilo ["$FILES"] satisfactoriamente"
	fi

	#Delete extracted files

	rm -f $FILES
	popd

fi
if [ ${VAR[0]} == "-r" ];then
	OUTPUT=$($RUN_FILE)
	LOG="Ejecuto el compilado"
fi
if [ ${VAR[0]} == "-lt" ];then
	OUTPUT=$(ls $TESTDIR)
	LOG="Checkeo los tests disponibles"
fi

#Write log
TIME=$(date +"%F %T")
echo -e $TIME" "$USER" "$LOG >> $LOG_FILE

#Print output to file
echo -e "$OUTPUT" >> $RESULT

#notify server you are done
echo "done" | nc $CGI_IP $CGI_PORT
echo $?
