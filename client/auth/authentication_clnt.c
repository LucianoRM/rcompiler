/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#include <memory.h> /* for memset */
#include "authentication.h"

/* Default timeout can be changed using clnt_control() */
static struct timeval TIMEOUT = { 25, 0 };

auth_response *
auth_1(authentication_args *argp, CLIENT *clnt)
{
	static auth_response clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call (clnt, AUTH,
		(xdrproc_t) xdr_authentication_args, (caddr_t) argp,
		(xdrproc_t) xdr_auth_response, (caddr_t) &clnt_res,
		TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

auth_response *
check_token_1(char **argp, CLIENT *clnt)
{
	static auth_response clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call (clnt, CHECK_TOKEN,
		(xdrproc_t) xdr_wrapstring, (caddr_t) argp,
		(xdrproc_t) xdr_auth_response, (caddr_t) &clnt_res,
		TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

auth_response *
create_user_1(authentication_args *argp, CLIENT *clnt)
{
	static auth_response clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call (clnt, CREATE_USER,
		(xdrproc_t) xdr_authentication_args, (caddr_t) argp,
		(xdrproc_t) xdr_auth_response, (caddr_t) &clnt_res,
		TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

auth_response *
delete_user_1(char **argp, CLIENT *clnt)
{
	static auth_response clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call (clnt, DELETE_USER,
		(xdrproc_t) xdr_wrapstring, (caddr_t) argp,
		(xdrproc_t) xdr_auth_response, (caddr_t) &clnt_res,
		TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}

auth_response *
get_users_1(void *argp, CLIENT *clnt)
{
	static auth_response clnt_res;

	memset((char *)&clnt_res, 0, sizeof(clnt_res));
	if (clnt_call (clnt, GET_USERS,
		(xdrproc_t) xdr_void, (caddr_t) argp,
		(xdrproc_t) xdr_auth_response, (caddr_t) &clnt_res,
		TIMEOUT) != RPC_SUCCESS) {
		return (NULL);
	}
	return (&clnt_res);
}
