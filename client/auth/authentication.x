struct authentication_args{
    string user<>;
	string pass<>;
};

union auth_response switch (int error) {
case 0:
    char data[1024];
default:
    char error_message[1024];
};


program AUTHENTICATE {
     version AUTHVERSION {
         	auth_response AUTH(authentication_args) = 1;
			auth_response CHECK_TOKEN(string token) = 2;
			auth_response CREATE_USER(authentication_args) = 3;
			auth_response DELETE_USER(string user) = 4;
			auth_response GET_USERS() = 5; 
      } = 1;
} = 0x20000001;
