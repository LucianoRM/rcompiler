 #!/bin/bash

source .config

#Help
ARGS=$#
TOKENFILE="token"
USERFILE="user"
RPC_CLIENT="./auth/authentication_client"
TOKEN=
USER=
HELP="$0
-c compile
-o local output file
-p platform <linux32/64-windows 32/64>
-r corre el compilado en el servidor
-lt lista los test disponibles
-lo deslogueo

Ejemlos:

$0 -c archivo.c  -o output.file


$0 -c lista archivos makefile -o output.file


$0 -p linux32 -c lista archivos makefile -o output.file


$0 -p linux32 -c lista archivos makefile -r -o output.file


$0 -lt"

#Print message and quit execution
function print_and_quit {
	echo -e "$1"
	exit
}

#Login
function login {

	#check if token exists and if it is correct
	check_token

	#if error in token, login
	if [ $? -eq 0 ]; then #token ok, leave
		return
	fi

	#ask for user and password
	read -p "Usuario: " USER
	read -s -p "Password: " PASS
	#call RPC client
	RESPONSE=$("$RPC_CLIENT" -l $USER $PASS)
	#Check if error
	if [ $? -ne 0 ] ;then #error
		print_and_quit "\n$RESPONSE"
	fi
	echo $USER > $USERFILE
	echo $RESPONSE > $TOKENFILE # save token
	TOKEN=$RESPONSE
}



#Check token
function check_token {

	#check if tokenfile exists
	if [ ! -f $TOKENFILE ] || [ ! -f $USERFILE ]; then
		return 1;
	fi

	#read token from file
	TOKEN=$(cat $TOKENFILE)
	USER=$(cat $USERFILE)

	#call RPC client
	RESPONSE=$("$RPC_CLIENT" -c $TOKEN)
	#Check if error
	if [ $? -ne 0 ] ;then #error
		return 1;
	fi
	return 0;
}


function parse_args {
	#$1 should be the flag $2 the args
	FLAG=$1
	ARGS=$2
	PARSED=$(echo $ARGS | sed "s/.*$FLAG//") #first remove other flags
	PARSED=$(echo $PARSED | sed "s/,-.*//") #if only one flag, remove -c
	#Replace , with space
	PARSED=$(echo $PARSED | sed 's/,/ /g')
	PARSED=$(echo $PARSED | sed 's/ $//g')
	echo "$PARSED"
}




###############Get Args###########################
FILES=
ARCHITECTURE="linux64"
OUTPUT=
TEST=
MAIN=
CANRUN=1
COMMA_SEPARATED_ARGS=$(echo "$@" | sed 's/ /,/g')
#add final comma to avoid losing last word
COMMA_SEPARATED_ARGS+=","
if [[ $* == *-c* ]]; then
	FILES=$(parse_args "-c" "$COMMA_SEPARATED_ARGS")
	MAIN=$(echo $FILES | awk '{print $1;}') #get first file
	#if no main, error
	grep -q "int main" $MAIN
	if [ $? -ne 0 ];then
		print_and_quit "-c: El primer archivo debe ser el que contiene a main()"
	fi

	MAIN=$(echo $MAIN | sed 's/.c//') #Remove .c
	OUTPUT=$MAIN

	#if multiple files, there should be a makefile
	NUMBER_OF_FILES=$(echo $FILES | wc -w)
	if [ $NUMBER_OF_FILES -eq 0 ];then
		print_and_quit "-c: No ser recibieron archivos"
	fi
	if [ $NUMBER_OF_FILES -gt 1 ]; then
		#if number of files greater than 1 then Makefile should exists
		echo $FILES | grep -q "Makefile"
		if [ ! $? -eq 0 ]; then #Makefile does not exists
			print_and_quit "-c: Se espera un Makefile al subir multiples archivos"
		fi

	fi
	#check if files exists
	for file in $FILES; do
		if [ ! -f "$file" ];then
			print_and_quit "-c: $file No existe"
		fi
	done

	if [[ $* == *-o* ]]; then
		OUTPUT=$(parse_args "-o" "$COMMA_SEPARATED_ARGS")
		NUMBER_OF_OUTPUT_FILES=$(echo $OUTPUT | wc -w)
		if [ $NUMBER_OF_OUTPUT_FILES -gt 1 ]; then
			print_and_quit "-o: Solo se espera 1 archivo de salida"
		fi
	fi
	if [[ $* == *-t* ]]; then
		TEST=$(parse_args "-t" "$COMMA_SEPARATED_ARGS")
		NUMBER_OF_TEST=$(echo $TEST | wc -w)
		if [ $NUMBER_OF_OUTPUT_FILES -gt 1 ]; then
			print_and_quit "-t: Solo se puede testear de a 1 test por vez"
		fi

	fi
	if [[ $* == *-p* ]]; then
		ARCHITECTURE=$(parse_args "-p" "$COMMA_SEPARATED_ARGS")
		NUMBER_OF_ARCHITECTURES=$(echo $ARCHITECTURE | wc -w)
		if [ $NUMBER_OF_ARCHITECTURES -gt 1 ]; then
			print_and_quit "-p: Solo se espera 1 plataforma"
		fi
		if [ ! $ARCHITECTURE = "linux32" ] && [ ! $ARCHITECTURE = "linux64" ] && [ ! $ARCHITECTURE = "windows32" ] && [ ! $ARCHITECTURE = "windows64" ]; then
			print_and_quit "-p: La plataforma no existe. Posibles: linux32/64,windows32/64"
		fi
	fi

fi



###START########
#if here is because all args were entered correctly and script can go on.

#first login
login

#if -c, zip all files and send to server
EXEC=0

if [[ $* == *-c* ]]; then
	EXEC=1
	#join all files
	tar -cf "$USER".tar $FILES > /dev/null

	#send files to nfs server and save response
	#SERVER_RESPONSE=$(curl -s -i -X POST -H "Content-Type: multipart/form-data" -H "User: $USER" -H "Architecture: $ARCHITECTURE" -H "Output: $MAIN" -F "upfile=@$USER.tar" -F "button=1" "$SERVER_IP"/cgi-bin/compile.cgi)
	RESPONSE_FILE="R"
	HEADER=$(curl -s -v -X POST -H "Content-Type: multipart/form-data" -H "User: $USER" -H "Token: $TOKEN" -H "Architecture: $ARCHITECTURE" -H "Output: $MAIN" -F "upfile=@$USER.tar" -F "button=1" "$SERVER_IP"/cgi-bin/compile.cgi -o $RESPONSE_FILE 2>&1)

	#Search error code in header
	echo -e "$HEADER" | grep -q "Error: 0"
	CANRUN=$?
	#echo -e "$HEADER"
	if [ $CANRUN -eq 0 ];then #No error
		echo -e "Compilacion OK!"
		#remove first byte to fix file received
		dd if="$RESPONSE_FILE" of="$OUTPUT" bs=1 skip=1	2>/dev/null
		chmod +x $OUTPUT
	else #Error, print result file
		echo -e "Error:"
		cat $RESPONSE_FILE
	fi

	#delete response file
	rm $RESPONSE_FILE
	#delete tar file
	rm "$USER".tar 2> /dev/null

	#if -r, run in remote server
	if [[ $* == *-r* ]]; then
		if [ $CANRUN -eq 0 ];then
			curl -s -H "User: $USER" -H "Token: $TOKEN" -H "Architecture: $ARCHITECTURE" -H "Output: $MAIN"  "$SERVER_IP"/cgi-bin/run.cgi
		fi
	fi

fi

#if -lt, print tests
if [[ $* == *-lt* ]];then
		EXEC=1
		curl -s -H "User: $USER" -H "Token: $TOKEN" "$SERVER_IP"/cgi-bin/list_tests.cgi
fi




#if -lo, log out
if [[ $* == *-lo* ]]; then
	EXEC=1
	if [ -f $TOKENFILE ]; then
		rm "$TOKENFILE"
	fi

	if [ -f $USERFILE ]; then
		rm "$USERFILE"
	fi
fi


if [ $EXEC -eq 0 ]; then
	print_and_quit "$HELP"
fi
