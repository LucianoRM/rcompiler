#!/bin/bash

source .config

pushd auth
OUTPUT="#ifndef _CONF_H_
#define _CONF_H_

#define HOST \"$SERVER_IP\"


#endif /* _CONF_H_ */"
echo -e "$OUTPUT" > conf.h
make clean
make
popd
