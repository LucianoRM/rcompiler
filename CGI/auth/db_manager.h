#ifndef _DB_MANAGER_H_
#define _DB_MANAGER_H_

#include <sqlite3.h>



enum db_error{OK,USER_NOT_FOUND,WRONG_PASS,NOT_LOGGED_IN,SESSION_EXPIRED,EXISTENT_USER};


int db_open(sqlite3** db,char* db_file);

int db_prepare_query(sqlite3* db,sqlite3_stmt** res,char* query_string);

int db_check_login(sqlite3* db,char* token); //Checks if the token belongs to active session

int db_check_pass(sqlite3* db,char* user,char* pass); //Checks if password is correct.

int db_add_token(sqlite3* db,char* user,char* token);

int db_add_timestamp(sqlite3* db,char* user);

int db_add_user(sqlite3* db,char* user,char* pass); //Adds a new user

int db_remove_user(sqlite3* db,char* user); //Removes an user

int db_get_users(sqlite3* db,char* usersBuffer); //Returns all the users in the database

int db_close(sqlite3* db);

void db_get_error_message(int error,char* buffer);

#endif /* _DB_MANAGER_H_ */
