#!/bin/bash
source .config

OUTPUT="<VirtualHost *:80>
	# The ServerName directive sets the request scheme, hostname and port that
	# the server uses to identify itself. This is used when creating
	# redirection URLs. In the context of virtual hosts, the ServerName
	# specifies what hostname must appear in the request's Host: header to
	# match this virtual host. For the default virtual host (this file) this
	# value is not decisive as it is used as a last resort host regardless.
	# However, you must set it for any further virtual host explicitly.
	#ServerName www.example.com

	ServerAdmin webmaster@localhost
	DocumentRoot \"$PATH_TO_CGI_FOLDER\"
	AddHandler cgi-script .cgi
	ScriptAlias /cgi-bin/ \"$CGI_SCRIPT_FOLDER\"
	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn


<Directory \"$PATH_TO_CGI_FOLDER\">
Require all granted
</Directory>
	ErrorLog \${APACHE_LOG_DIR}/error.log
	CustomLog \${APACHE_LOG_DIR}/access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with \"a2disconf\".
	#Include conf-available/serve-cgi-bin.conf
</VirtualHost>"


echo -e "​LoadModule cgi_module $PATH_TO_CGI_MODULE" >> $PATH_TO_APACHE_CONF_FILE
echo -e "$OUTPUT" > "$APACHE_CONF_FOLDER/001-server.conf"

mount $NFS_REMOTE_IP:$NFS_REMOTE_FOLDER $NFS_LOCAL_FOLDER

pushd client

OUTPUT="#ifndef _CONN_CONF_H_
#define _CONN_CONF_H_

#define SERVER_CLIENT_LISTENER_IP \"$SERVER_CLIENT_LISTENER_IP\"
#define SERVER_CLIENT_LISTENER_PORT $SERVER_CLIENT_LISTENER_PORT


#endif /* _CONN_CONF_H_ */"

echo -e "$OUTPUT" > conn_conf.h
make clean
make
popd

pushd auth
OUTPUT="#ifndef _CONF_H_
#define _CONF_H_

#define HOST \"$AUTHENTICATION_SERVER_IP\"


#endif /* _CONF_H_ */"
echo -e "$OUTPUT" > conf.h
make clean
make
popd

service apache2 start

chmod 777 $CGI_SCRIPT_FOLDER
chmod 777 $NFS_LOCAL_FOLDER
chmod 777 "./auth"
chmod 777 "./client"
