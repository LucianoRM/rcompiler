#include "tcp.h"
#include "conf.h"
#include "conn_conf.h"


int main(int argc, char* argv[]) {
	//Ingore writeerror signals
	signal(SIGPIPE,SIG_IGN);

	//Check input
	if(argc <= 1){
		printf("No parameters");
		return 1;
	}

	//Create socket
	int socketfd = tcp_open_activo(SERVER_CLIENT_LISTENER_IP,SERVER_CLIENT_LISTENER_PORT);

	//Check for errors
	if(socketfd == -1){
		printf("Error connecting to server\n");
		perror(NULL);
		return 1;
	}

	//At this point, it should be connected to server
	char block[BLOCKSIZE];


	//When compiling should be -c user architecture output
	//when testing should be -r user file architecture
	//when printing test should be -lt user

	if(!strcmp(argv[1],"-c")){
		sprintf(block,"%s %s %s %s",argv[1],argv[2],argv[3],argv[4]);
	}else if(!strcmp(argv[1],"-r")){
		sprintf(block,"%s %s %s %s",argv[1],argv[2],argv[3],argv[4]);
	}else{ //-lt
		sprintf(block,"%s %s",argv[1],argv[2]);
	}

	//Send to server
	write(socketfd,block,BLOCKSIZE);
	perror("Error\n");

	close(socketfd);


	return 0;
}
