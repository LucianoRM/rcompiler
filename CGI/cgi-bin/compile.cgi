#!/usr/bin/perl
#
# Author: Kyle Dent
# Date: 3/15/01
#

use CGI;
use strict;
#use Linux::Inotify2;

my $PROGNAME = "compile.cgi";
my $nfs = "../shared";
my $client = "../client/client";
my $auth = "../auth/authentication_client";
my $port = "8082";


my $cgi = new CGI();


#
# If we're invoked directly, display the form and get out.
#
if (! $cgi->param("button") ) {
	DisplayForm();
	exit;
}

#
# We're invoked from the form. Get the filename/handle.
#
my $upfile = $cgi->param('upfile');

#
# Get the basename in case we want to use it.
#
my $basename = GetBasename($upfile);

#
# At this point, do whatever we want with the file.
#
# We are going to use the scalar $upfile as a filehandle,
# but perl will complain so we turn off ref checking.
# The newer CGI::upload() function obviates the need for
# this. In new versions do $fh = $cgi->upload('upfile');
# to get a legitimate, clean filehandle.
#
no strict 'refs';

#######################################################
# Choose one of the techniques below to read the file.
# What you do with the contents is, of course, applica-
# tion specific. In these examples, we just write it to
# a temporary file.
#
# With text files coming from a Windows client, probably
# you will want to strip out the extra linefeeds.
########################################################

#
# Write file
#
my $filename = $cgi->http("User").".tar";
my $architecture = $cgi->http("Architecture");
my $user = $cgi->http("User");
my $output = $cgi->http("Output");
my $token = $cgi->http("Token");



#Check for successful login
system($auth." -c"." ".$token."> /dev/null");
my $login_result = $?;

if ($login_result != 0) {
	system("echo ".$user." > r");
	print "Content-type: application/octet-stream\n";
	print "Error: ".$login_result."\n";
	print "\n\n"; #Body starts here
	print "Error de logueo\n";
	exit;

}


system("rm -rf ".$nfs."/".$user."/*"); #Delete previous folder in case there are old files
umask(0);
mkdir($nfs."/".$user);

if (! open(OUTFILE, ">","./".$nfs."/".$user."/".$filename) ) {
	print "Content-type: application/octet-stream\n";
	print "Error: 1\n";
	print "\n\n";
	print "Error en servidor\n";
	exit(-1);
}


my $nBytes = 0;
my $totBytes = 0;
my $buffer = "";
# If you're on Windows, you'll need this. Otherwise, it
# has no effect.
binmode($upfile);
#binmode($fh);
while ( $nBytes = read($upfile, $buffer, 1024) ) {
#while ( $nBytes = read($fh, $buffer, 1024) ) {
	print OUTFILE $buffer;
	$totBytes += $nBytes;
}

close(OUTFILE);

#
# Turn ref checking back on.
#
use strict 'refs';

#Create result file
my $result_file = $nfs."/".$user."/"."result";
open(my $fh, ">",$result_file);
close($fh);


## Send request to server
system($client." -c"." ".$user." ".$architecture." ".$output);


#Wait for change in result_file

#my $notify = "inotifywait -e modify ".$result_file." > /dev/null 2> /dev/null";
#system($notify);

my $notify = "nc -l -p ".$port." > /dev/null 2> /dev/null";
my $notify_code = system($notify);

if($notify_code != 0){
    print "Error: $notify_code\n";
    print "\n\n"; #Error message body
    print "Error con la conexion, intentelo de nuevo\n";
    exit(-1);
}


#Write response header
print "Content-type: application/octet-stream\n";
open(my $fh, '<:encoding(UTF-8)', $result_file);

#Get error code
my $row = <$fh>;
chomp $row;
print "Error: $row\n";

print "\n\n"; #Body starts here

#If error, error message would be in body. If not, compiled file would be in body
if($row != 0){ #error
	while (my $row = <$fh>) {
		chomp $row;
		print "$row\n";
	}
}else{ #No error, should return compiled file
	open(my $cf, '<', $nfs."/".$user."/".$output);
	my $buffer;
	binmode ($cf);
	while ( (read ($cf, $buffer, 65536)) != 0 ) {
		print $buffer;
	}
	close($cf);
}

close($fh);
unlink($result_file);







##############################################
# Subroutines
##############################################

#
# GetBasename - delivers filename portion of a fullpath.
#
sub GetBasename {
	my $fullname = shift;

	my(@parts);
	# check which way our slashes go.
	if ( $fullname =~ /(\\)/ ) {
		@parts = split(/\\/, $fullname);
	} else {
		@parts = split(/\//, $fullname);
	}

	return(pop(@parts));
}

#
# DisplayForm - spits out HTML to display our upload form.
#
sub DisplayForm {
print <<"HTML";
<html>
<head>
<title>Upload Form</title>
<body>
<h1>Upload Form</h1>
<form method="post" action="$PROGNAME" enctype="multipart/form-data">
<center>
Enter a file to upload: <input type="file" name="upfile"><br>
<input type="submit" name="button" value="Upload File">
</center>
</form>

HTML
}
