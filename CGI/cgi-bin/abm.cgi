#!/bin/bash
AUTH="../auth/authentication_client"
USER=$HTTP_USER
PASS=$HTTP_PASS
OPERATION=$HTTP_OPERATION
TOKEN=$HTTP_TOKEN




#Check if it is a valid user
RESPONSE=$($AUTH -c $TOKEN)

LOGIN_SUCCESSFUL=$?

if [ $LOGIN_SUCCESSFUL -ne 0 ]; then

  echo -e "Error: $LOGIN_SUCCESSFUL"
  #Start response body
  echo -e "\n\n"
	echo $RESPONSE
	exit
fi

####Check operation!!!!

if [ $OPERATION != '-a' || $OPERATION != '-r' ]; then
  echo -e "Error: 5\n"
  echo -e "\n\n"
  echo -e "Operacion invalida\n"
  exit
fi



#Do modification
RESPONSE=$($AUTH $OPERATION $USER $PASS)

MOD_SUCCESSFUL=$?

if [ $MOD_SUCCESSFUL -ne 0 ]; then
  echo -e "Error: $MOD_SUCCESSFUL"
  #Start response body
  echo -e "\n\n"
	echo $RESPONSE
	exit
fi

echo -e "Error: 0\n"
echo -e "\n\n"
echo -e "$RESPONSE\n"
