#!/bin/bash
AUTH="../auth/authentication_client"
USER=$HTTP_USER
PASS=$HTTP_PASS   #If there is no pass, then check token and the other way around
TOKEN=$HTTP_TOKEN



#Login
function login {

	#call RPC client
	RESPONSE=$("$AUTH" -l $USER $PASS)

  LOGIN_SUCCESSFUL=$?

	#Check if error
	if [ $LOGIN_SUCCESSFUL -ne 0 ] ;then #error
		echo -e "Error: $LOGIN_SUCCESSFUL\n"
    #Start response body
    echo -e "\n\n"
    echo -e $RESPONSE
    exit
	fi
	echo -e "Error: $LOGIN_SUCCESSFUL"
  echo -e "Token: $RESPONSE\n"

  #Start response body
  echo -e "\n\n"
}



#Check token
function check_token {

	#call RPC client
	RESPONSE=$("$AUTH" -c $TOKEN)

  TOKEN_CHECK_SUCCESFUL=$?

	#Check if error
	if [ $TOKEN_CHECK_SUCCESFUL -ne 0 ] ;then #error
    echo -e "Error: $TOKEN_CHECK_SUCCESFUL\n"
    #Start response body
    echo -e "\n\n"
    echo -e $RESPONSE
    exit
	fi
  #Start response body
	echo -e "Error: $TOKEN_CHECK_SUCCESFUL"
  echo -e "\n\n"
  echo -e "OK"
}

#Check if there is a token then check it, if there is not, check pass
if [ "$TOKEN" != "" ] ;then
  check_token
  exit
fi

#else
login
exit
