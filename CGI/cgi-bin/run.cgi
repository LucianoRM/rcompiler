#!/bin/bash
NFS="../shared";
CLIENT="../client/client"
AUTH="../auth/authentication_client"
USER=$HTTP_USER
OUTPUT=$HTTP_OUTPUT
ARCHITECTURE=$HTTP_ARCHITECTURE
RESULT_FILE=$NFS"/"$USER"/""result"
TOKEN=$HTTP_TOKEN
PORT=8082



#Check if it is a valid user
RESPONSE=$($AUTH -c $TOKEN)

LOGIN_SUCCESSFUL=$?

if [ $LOGIN_SUCCESFUL -ne 0 ]; then
	echo $RESPONSE
	exit
fi

#Print first line in file
echo -e "Salida:" > $RESULT_FILE
chmod 777 $RESULT_FILE


#Call worker
$CLIENT -r $USER $OUTPUT $ARCHITECTURE

#wait for result to change
nc -l -p $PORT > /dev/null 2> /dev/null
NOTIFY_CODE=$?

if [ $NOTIFY_CODE -ne 0 ]; then
    echo -e "Error: $NOTIFY_CODE\n"
    echo -e "\n\n" #Error message body
    echo -e "Error con la conexion, intentelo de nuevo\n"
    exit
fi


#print answer
#Start response body
echo -e "\n\n"

cat $RESULT_FILE

#remove result file
rm $RESULT_FILE
