#!/bin/bash
NFS="../shared";
CLIENT="../client/client"
TESTS_FOLDER=$NFS"/"test
USER=$HTTP_USER
RESULT_FILE=$NFS"/"$USER"/""result"

#Start response body
echo -e "\n\n"

#Create user dir
mkdir $NFS"/"$USER
chmod 777 $NFS"/"$USER

#Print first line in file
echo -e "Tests Disponibles:" > $RESULT_FILE
chmod 777 $RESULT_FILE


#Call worker
$CLIENT -lt $USER

#wait for result to change
inotifywait -e modify "$RESULT_FILE" > /dev/null 2> /dev/null

#print answer
cat $RESULT_FILE

#remove result file
rm $RESULT_FILE


