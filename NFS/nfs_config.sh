#!/bin/bash
source .config

echo -e "#Nombre del directorio a exportar\n$DIR *(insecure,rw,no_root_squash,fsid=0)" > /etc/exports
service rpcbind start
service nfs-kernel-server start
exportfs -a
