#this script handles the connection from the webserver to the apache server
import requests
import tarfile

def download_file(r,filename):
    with open("tmp/"+ filename, 'wb') as f:
        f.write(r.content[1:]) #dont save first byte
    return

def compile(folder,user,token,architecture,output):
     header = {'User': user, 'Token': token, 'Architecture': architecture, 'Output': output, 'Content-Type' : 'multipart/form-data'}
     files = {'upfile': open('tmp/' + user + "/" + user + '.tar', 'rb'), 'button' : '1'}
     response = requests.post('http://localhost/cgi-bin/compile.cgi', files=files,headers=header)
     download_file(response,output)
     return response

def login_with_pass(user,password):
    header = {'User': user, 'Pass': password }
    response = requests.post('http://localhost/cgi-bin/login.cgi',headers=header)
    error_value = response.headers['Error']
    if(int(error_value) != 0):
        return (error_value,response.text)
    return (error_value,response.headers['Token'])


def login_with_token(user,token):
     header = {'User': user, 'Token': token }
     response = requests.post('http://localhost/cgi-bin/login.cgi',headers=header)
     return response.headers['Error']

def abm(user,password,operation):
    header = {'User': user, 'Pass': password, 'Operation': operation }
    response = requests.post('http://localhost/cgi-bin/abm.cgi',headers=header)
    error_value = response.headers['Error']
    return (error_value,response.text)
