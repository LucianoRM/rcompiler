from flask import Flask, render_template, request, session, redirect, url_for, send_file,flash, Markup
from flask_uploads import UploadSet, configure_uploads, SCRIPTS
import server_connection
import tarfile
import os

app = Flask(__name__)
app.secret_key = "thiskeyshouldberandom"

ADMIN_USER='luciano'

#Allow only scripts uploads
scripts = UploadSet('scripts', ('o','c','h',''))

TARGET_DIR = 'tmp/'

#Set target upload directory
app.config['UPLOADED_SCRIPTS_DEST'] = TARGET_DIR
configure_uploads(app, scripts)

if not os.path.exists(TARGET_DIR):
    os.makedirs(TARGET_DIR)

def is_logged_in():
    if ('logged_in' not in session or session['logged_in'] == False):
        return False
    if('user' not in session or 'token' not in session):
        return False
    error = server_connection.login_with_token(session['user'],session['token'])
    if(int(error)):
        return False
    return True

def is_admin():
    if (is_logged_in() == False):
        return False
    if(session['user'] != ADMIN_USER):
        return False
    return True


def are_files_ok(fileList):
    if(len(fileList) > 1): #shuld have Makefile
        if(True in map(lambda x: x.filename == 'Makefile',fileList)):
            return True
        return False
    return True

def create_tar(username):
    path = "tmp/"+username
    files = os.listdir(path)
    tFile = tarfile.open(path + "/" +username+".tar", 'w')
    for f in files:
        tFile.add(path + '/' + f,f)
        os.remove(path+'/'+f)
    tFile.close()

def empty_tmp(dirname):
    path = dirname
    files = os.listdir(path)
    for file in files:
        try:
            os.remove(dirname +"/"+ file)
        except:
            try:
                os.rmdir(dirname +"/"+ file)
            except:
                empty_tmp(dirname +"/"+ file)

@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/users' , methods = ['GET','POST'])
def users():
    if(not is_admin()):
        return redirect('upload')
    if(request.method == 'POST'):
        try: #this block is executed if add user is pressed
            request.form['add-user']
            newUsername = request.form['username']
            newPassword = request.form['password']
            (error_value,result) = server_connection.abm(newUsername,newPassword,'-a')
            if(int(error_value) != 0):
                return render_template('users.html',error=result)
        except: #this block is executed if remove is pressed
            form_keys = request.form.keys()
            user_to_delete_list = filter(lambda x: x != "username" and x != "password",form_keys)
            (error_value,result) = server_connection.abm(user_to_delete_list[0],None,'-r')
            if(int(error_value) != 0):
                return render_template('users.html',error=result)
    (error_value,result) = server_connection.abm(None,None,'-q') #List users
    if(int(error_value) != 0):
        return render_template('users.html',error="Error de conexion")
    users = result.strip().split("\n")
    for user in users:
        if(user == session['user']):
            continue;
        message = Markup(user)
        flash(message)
    return render_template('users.html')



@app.route('/upload', methods=['GET', 'POST','PATCH'])
def upload():
    if(is_logged_in() == False):
        return redirect('login')
    if request.method == 'POST':
        file_list = request.files.getlist("files[]")
        if(not are_files_ok(file_list)):
            return render_template('upload.html',error='Se necesita Makefile para multiples archivos')
        user = session['user']
        token = session['token']
        architecture = request.form['selectArq']
        output = request.form['outputName']
        empty_tmp('tmp')
        try:
            for file in file_list:
                scripts.save(file,user)
        except:
            return render_template('upload.html',error="Extensiones de archivos invalidas")
        create_tar(user)
        result = server_connection.compile("tmp/",user,token,architecture,output)
        if(int(result.headers['Error'])):
            return render_template('upload.html',error=result.text)
        return send_file('tmp/' + output,None,True,output)
    return render_template('upload.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    #if uploading files
    if request.method == 'POST':
        user = request.form['form-username']
        password = request.form['form-password']
        (error_value,data) = server_connection.login_with_pass(user,password)
        if(int(error_value)):
            return render_template('login.html', error=data)
        else:
            session['user'] = user
            session['token'] = data
            session['logged_in'] = True
            return redirect('upload')
    return render_template('login.html')

@app.route('/logout')
def logout():
    session['logged_in'] = False
    return redirect('login')




if __name__ == '__main__':
	app.run(debug=True)
