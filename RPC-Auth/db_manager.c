#include "db_manager.h"
#include "logger.h"
#include <time.h>


//Open DB

int db_open(sqlite3** db,char* db_file){

	int rc = sqlite3_open(db_file, db);
	if(rc != SQLITE_OK){
        safelog("sqlite3 error. could not open db\n %s", sqlite3_errmsg(*db));
        exit(1);
    }
	return 0;

}

int db_prepare_query(sqlite3* db,sqlite3_stmt** res,char* query_string){

	//Prepare statement
	int rc = sqlite3_prepare_v2(db, query_string, -1, res, 0);

	if(rc != SQLITE_OK){
        safelog("sqlite3 error.could not execute query\n %s", sqlite3_errmsg(db));
        exit(1);
    }

	return 0;

}


int db_check_pass(sqlite3* db,char* user,char* pass){


	int result;
	//Create statement
	char query_string[512];
	sprintf(query_string,"SELECT PASS FROM USERS WHERE USER = '%s';",user);

	//Preapare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	//Query
	if(sqlite3_step(res) == SQLITE_ROW){
		if(!strcmp(sqlite3_column_text(res, 0),pass)){
			db_add_timestamp(db,user);
			result = OK; //Password is equal
		}else{
			result = WRONG_PASS;//Password is wrong
		}
	}else{
		result = USER_NOT_FOUND; //User not found
	}

    sqlite3_finalize(res);

	return result;


}

int db_check_login(sqlite3* db,char* token){


	int result;

	//Create statement
	char query_string[512];
	sprintf(query_string,"SELECT TIMESTAMP FROM USERS WHERE TOKEN = '%s';",token);

	//Preapare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	//Generate current timestamp
	time_t actual_time;
	time(&actual_time);

	//Query
	if(sqlite3_step(res) == SQLITE_ROW){
		//Get saved time
		int saved_time = atoi(sqlite3_column_text(res, 0));
		if(((int)actual_time - saved_time) < 3600){ //More than an hour since last loggin
			result = OK;
		}else{
			result = SESSION_EXPIRED;
		}
	}else{
		result = NOT_LOGGED_IN;
	}


    sqlite3_finalize(res);

	return result;

}

int db_add_token(sqlite3* db,char* user,char* token){

	int result;

	//Create statement
	char query_string[512];
	sprintf(query_string,"UPDATE USERS SET TOKEN = '%s' WHERE USER = '%s';",token,user);

	//Preapare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	//Query
	if(sqlite3_step(res) == SQLITE_DONE){
		result = OK;
	}else{
		result = USER_NOT_FOUND;
	}


    sqlite3_finalize(res);

	return result;

}

int db_add_timestamp(sqlite3* db,char* user) {

	int result;

	//Generate current timestamp
	time_t actual_time;
	time(&actual_time);

	//Create statement
	char query_string[512];
	sprintf(query_string,"UPDATE USERS SET TIMESTAMP = '%d' WHERE USER = '%s';",(int)actual_time,user);

	//Preapare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	//Query
	if(sqlite3_step(res) == SQLITE_DONE){
		result = OK;
	}else{
		result = USER_NOT_FOUND;
	}


    sqlite3_finalize(res);

	return result;
}

int db_add_user(sqlite3* db,char* user,char* pass) {

	int result;

	//Create statement
	char query_string[512];
	sprintf(query_string,"INSERT INTO USERS(USER,PASS,TOKEN,TIMESTAMP) VALUES ('%s','%s',0,0)",user,pass);

	//Preapare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	//Query
	if(sqlite3_step(res) == SQLITE_DONE){
		result = OK;
	}else{
		result = EXISTENT_USER;
	}

	sqlite3_finalize(res);

	return result;

}

int db_remove_user(sqlite3* db,char* user) {

	int result;

	//Create statement
	char query_string[512];
	sprintf(query_string,"DELETE FROM USERS WHERE USER = '%s';",user);

	//Preapare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	//Query
	if(sqlite3_step(res) == SQLITE_DONE){
		result = OK;
	}else{
		result = USER_NOT_FOUND;
	}

	sqlite3_finalize(res);

	return result;


}

int db_get_users(sqlite3* db,char* usersBuffer) {
	int result;

	//Create statement
	char query_string[512];
	sprintf(query_string,"SELECT USER FROM USERS;");

	//Prepare query
	sqlite3_stmt* res;
	db_prepare_query(db,&res,query_string);

	usersBuffer[0] = 0;

	int a = sqlite3_step(res);
	while(a == SQLITE_ROW) {
		char* s = sqlite3_column_text(res, 0);
		strcpy(usersBuffer + strlen(usersBuffer), s);
		strcpy(usersBuffer + strlen(usersBuffer), "\n");
		a = sqlite3_step(res);
	}


	if(a == SQLITE_DONE){
		result = OK;
	}else{
		result = -1;
	}

	sqlite3_finalize(res);

	return result;


}




int db_close(sqlite3* db){

	//Close db
	sqlite3_close(db);

}


void db_get_error_message(int error,char* buffer){

	switch(error){
		case OK:
			strcpy(buffer,"OK\n");
			break;
		case USER_NOT_FOUND:
			strcpy(buffer,"El usuario no existe\n");
			break;
		case WRONG_PASS:
			strcpy(buffer,"Password incorrecto\n");
			break;
		case NOT_LOGGED_IN:
			strcpy(buffer,"El usuario no esta logeado\n");
			break;
		case SESSION_EXPIRED:
			strcpy(buffer,"La sesion expiro\n");
			break;
		case EXISTENT_USER:
			strcpy(buffer,"Usuario existente\n");
			break;
		default:
			strcpy(buffer,"Error en la base de datos\n");
			break;
	}

}


