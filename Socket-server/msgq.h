/*Messsage Queue library*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include "conf.h"


typedef struct message{
	long mtype;
	char text[BLOCKSIZE];

} message ;




//Get key from string
key_t get_key(char* keystring,int number){
	
	key_t key = ftok(keystring,number);
	
	if(key == -1){
		perror("ftok error: could not generate queue key\n.");
		exit(1);	
	}
	
	return key;
	
}



//Create a new queue
int q_create(char* keystring,int number){
	
	//Get key
	key_t key = get_key(keystring,number);
	
	//Create queue. Error if queue already exists.
	int q_id = msgget(key, IPC_CREAT | IPC_EXCL |0666 );
	
	if(q_id == -1){
		perror("msgget error: could not create queue\n");
		exit(1);
	}
	
	return q_id;
	
}


//Get existing queue
int q_get(char* keystring,int number){
	
	//Get key
	key_t key = get_key(keystring,number);
	
	//Get queue. Error if queue doesn't exists
	int q_id = msgget(key, 0666 );
	
	if(q_id == -1){
		perror("msgget error: could not get queue\n");
		exit(1);
	}
	
	return q_id;
	
}

//Send message to queue
int q_send(int q_id,message* msg){
	
	int cd = msgsnd(q_id,msg,sizeof(message)-sizeof(long),0);
	
	if(cd == -1){
		perror("msgsnd error: could not send message\n");
		exit(1);
	}
	
	return 0;
}


//Receive message from queue
int q_receive(int q_id,message* msg,long type){
	
	
	size_t size = msgrcv(q_id,msg,sizeof(message)-sizeof(long),type,0);
		
	if(size == -1){
		perror("msgrcv error: could not receive message\n");
		exit(1);
	}
	
	return 0;
}


//Delete queue
int q_delete(int q_id){
	
	msgctl(q_id,IPC_RMID,0);

	
}

