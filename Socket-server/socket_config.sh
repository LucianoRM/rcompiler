#!/bin/bash

source .config

OUTPUT="#ifndef _CONN_CONF_H_
#define _CONN_CONF_H_

#define SERVER_CLIENT_LISTENER_PORT $SERVER_CLIENT_LISTENER_PORT
#define SERVER_WORKER_LISTENER_PORT $SERVER_WORKER_LISTENER_PORT



#endif /* _CONN_CONF_H_ */"

echo -e "$OUTPUT" > conn_conf.h

make clean
make

./server_starter

./server_worker_listener &
./server_client_listener &
