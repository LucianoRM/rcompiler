/*Deletes every ipcs created by the server*/

#include "msgq.h"
#include "conf.h"

int main(){
	
	//Get msgq
	int msg_q = q_get(QUEUEFILEKEY,QUEUECHARKEY);
	
	//Delete queue
	q_delete(msg_q);
	
	return 0;
	
	
	
}
