#include "conf.h"
#include "msgq.h"
#include "tcp.h"







int main(int argc, char* argv[]){

	

    if(argc<2){
        printf("%s <fd>\n", argv[0]);
        printf("Error\n");
        return -1;
    }
    
    //Socketfd of remote worker
    int socketfd = atoi(argv[1]);
    
    //Open job queue
    int q_id = q_get(QUEUEFILEKEY,QUEUECHARKEY);
    
    //Read message from worker and save server architecture
	char c_arch[2];
    tcp_read_or_die(socketfd,c_arch,2);
    
    int architecture = atoi(c_arch);
        
    char arq[10];

    switch(architecture){
	case LINUX32:
	    sprintf(arq,"linux32");
	    break;
	case LINUX64:
	    sprintf(arq,"linux64");
	    break;
	case WINDOWS32:
	    sprintf(arq,"windows32");
	    break;
	case WINDOWS64:
	    sprintf(arq,"windows64");
	    break;
	default:
	    break;
    }

    printf("Worker conectado, arquitectura: %s\n",arq);

    while(1){
		//Read from queue
		message msg;
		q_receive(q_id,&msg,(long)architecture);
		
		
		//Send job to remote worker
		int res = write(socketfd,msg.text,BLOCKSIZE);

		/*//This part is done because inotify does not work with NFS unless the files are modified from the same computer
		//Get ack from socket
		char ack[2];
		tcp_read_or_die(socketfd,ack,2);

		FILE* f = fopen(PATH,"a");

		if (f == NULL){
    		printf("Error opening file!\n");
    		exit(1);
		}
		fprintf(f, "\n");

		fclose(f);
		printf("Termine de escribir el archivo!\n");
		*/
		
	}
    
    
    //Close socket with remote worker
	close(socketfd);
	return 0;
}

