#include "conf.h"
#include "conn_conf.h"
#include <stdio.h>
#include "msgq.h"
#include "tcp.h"
#include <string.h>



/*Handles connections from clients. Receives jobs and puts them inside queue*/


int main(){

	//Open queue

	int q_id = q_get(QUEUEFILEKEY,QUEUECHARKEY);

	char* input = NULL;

	int fd = tcp_open_pasivo(SERVER_CLIENT_LISTENER_PORT);
    if(fd < 0){
		printf("Error opening server socket-client listener\n");
		return 1;
	}
	printf("Listening on %d\n",SERVER_CLIENT_LISTENER_PORT);



	char message_from_client[BLOCKSIZE];
	while(1){
		int client_fd = accept(fd, (struct sockaddr*) NULL, NULL);
		if(client_fd < 0){
			printf("Error accepting client\n");
			return 1;
		}



		tcp_read_or_die(client_fd,message_from_client,BLOCKSIZE);


		message msg;


		msg.mtype = LINUX32; //Default architecture

		//Check target architecture
		if(strstr(message_from_client,"linux64") != NULL){
			msg.mtype = LINUX64;
		}else if(strstr(message_from_client,"windows32") != NULL){
			msg.mtype = WINDOWS32;
		}else if(strstr(message_from_client,"windows64") != NULL){
			msg.mtype = WINDOWS64;
		}

		strcpy(msg.text,message_from_client);
		q_send(q_id,&msg);
		close(client_fd);
	}

	return 0;

}
