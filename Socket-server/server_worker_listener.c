/*Receives the request to connect from the workers and registers them as new workers*/

//remote workers are the ones requesting the connections
//Indians belong to the local server and are linked with a worker.

#include "tcp.h"
#include "conn_conf.h"
#include "conf.h"



int main(){

	//Open new connections socket
    int fd = tcp_open_pasivo(SERVER_WORKER_LISTENER_PORT);
    if(fd < 0){

		perror("Error opening server socket-worker listener\n");
		return 1;
	}


    printf("Listening on %d\n",SERVER_WORKER_LISTENER_PORT);
    while(1){

        int remote_worker_fd = accept(fd, (struct sockaddr*) NULL, NULL);
        if(remote_worker_fd < 0){
			printf("Error accepting client\n");
			return 1;
		}

		//Save remote_worker_fd
        static char fd_string[10];
        snprintf(fd_string, 10, "%d", remote_worker_fd);

        int pid = fork();
        if(pid == 0){
            close(fd);
            execl("./server_indian", "server_indian", fd_string, (void*)0);
            exit(-1);//If here is because execv had an error
        }
        close(remote_worker_fd);
    }
}
